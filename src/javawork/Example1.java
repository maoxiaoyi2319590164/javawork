package javawork;
//1 创建一个类，继承自Thread
//2 在类中，实现run方法
//3 在主线程创建类的对象，调用start方法气功线程
class MyThread extends Thread{
	public void run() {
		while(true) {
			System.out.println("子线程运行");
		}
		
	}
	
}
public class Example1 {

	public static void main(String[] args) {
		MyThread thread = new MyThread();
		thread.start();
		while(true) {
			System.out.println("主线程！！！！");
			
		}
		
	}

}
