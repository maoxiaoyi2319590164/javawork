package test;
import java.util.ArrayList;
import java.util.Scanner;

public class KTV {
	public static void main(String[] args) {
		System.out.println("----------欢迎来到点歌系统----------");
		System.out.println("1.添加歌曲至列表");
		System.out.println("2.将歌曲置顶");
		System.out.println("3.将歌曲前移一位");
		System.out.println("4.退出");
		/*创建一个存储歌曲的列表的集合*/ 
		ArrayList<String> list = new ArrayList<String>();
		/*添加一部分歌曲至歌曲列表中*/ 
		addMusicList(list);
		while(true){
			System.out.print("请输入要执行的操作序号：");
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			int command = sc.nextInt(); /*接受序号*/ 
			switch (command) {
				case 0:
					addMusic(list);
					break;
				case 1:
					setTop(list);
					break;
				case 2:
					steBefore(list);
					break;
				case 3:
					exit();
					break;
				default:
					System.out.println("输入有误，请输入正确的功能序号");
					break;
				}
			System.out.println("当前歌曲列表" + list);
		}
	}
	
	/*初始歌曲名称*/ 
	private static void addMusicList(ArrayList<String> list) {
		list.add("晚安");
		list.add("我的名字");
		list.add("蜀绣");
		list.add("男孩");
		list.add("春风十里");
		list.add("好久不见");
		System.out.println("初始歌曲列表："+list);
	}
	/*添加歌曲*/ 
	private static void addMusic(ArrayList<String> list) {
		System.out.println("请输入要添加的歌曲名称");
		@SuppressWarnings("resource")
		String musicName = new Scanner(System.in).nextLine();
		list.add(musicName);
		System.out.println("以添加歌曲："+musicName);
	}
	
	/*将歌曲置顶*/
	private static void setTop(ArrayList<String> list) {
		System.out.println("请输入要置顶的歌曲");
		@SuppressWarnings("resource")
		String musicName = new Scanner(System.in).nextLine();
		int position = list.indexOf(musicName);  /*查找指定歌曲的位置*/
			if(position==-1) { 
				/*判断输入数是否存在*/ 
				System.out.println("当前列表中没有该歌曲");
			} else {
				list.remove(musicName);/*移除指定的歌曲*/
				list.add(0,musicName);/*第一插入该歌曲    ArrayList没有addFirst方法，所以用索引*/
				System.out.println("已将歌曲"+musicName+"置顶");
			}
	}
	
	/*歌曲前移一位*/
	private static void steBefore(ArrayList<String> list) {
		System.out.println("请输入要前移一位的歌曲");
		@SuppressWarnings("resource")
		String musicName = new Scanner(System.in).nextLine();
		int position = list.indexOf(musicName);
			if(position ==-1) {
				System.out.println("当前列表没有该歌曲");
			} else if(position==0) { /*判断是否在第一位*/
				System.out.println("该歌曲已经是第一位了");
			} else {
			/*移除指定的歌曲*/
			list.remove(musicName);
			/*添加该歌曲至原来的位置的前一位*/
			list.add(position-1, musicName);
		}
			System.out.println("已将"+musicName+"前移一位");	
	} 
	
	/*退出*/
	private static void exit() {
		System.out.println("----------退出----------");
		System.out.println("您已经退出系统");
		System.exit(0); /*结束正在运行的Java程序*/
	}
}
