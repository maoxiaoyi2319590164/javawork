package gaojibiancheng;
class Student {// 创建Student这个类
private int no;// 成员变量
private static int age;
private String name;
private String sex;
public void setAge(int myAge) {//封装
if (myAge <= 0) {
System.out.println("年龄出错,使用默认年龄18岁代替");
age=18;
}
else
age=myAge;
}
public int getNo() {
System.out.println("学号："+no);
return no;
}
public String getName(){
System.out.println("姓名："+name);
return name;}
public String getSex(){
System.out.println("性别："+sex);
return sex;}
public int getAge() {
System.out.println("年龄："+age);
return age;
}


public Student(int myNo, String myName, String mySex, int myAge) {//构造方法变量初始化
no = myNo;
name = myName;
sex = mySex;
age = myAge;
}

public Student(int myNo, String myName, String mySex) {
no = myNo;
name = myName;
sex = mySex;
}

public Student(int myNo, String myName) {
no = myNo;
name = myName;
}

public Student(int myNo) {
no = myNo;
}
public static void main(String[] args) { // 声明对象和创建对象
Student Student1= new Student(1625,"张三","男",20);
Student Student2= new Student(1624,"李四","男",18);
Student Student3= new Student(1623,"小明","男",18);
Student Student4= new Student(1622,"肖丽");
Student1.setAge(age);
Student1.getNo();
Student1.getName();
Student1.getSex();
Student1.getAge();
Student2.getNo();
Student2.getName();
Student2.getSex();
Student3.getNo();
Student3.getName();
Student4.getNo();
Student4.getName();
Student4.getSex();

}


}
