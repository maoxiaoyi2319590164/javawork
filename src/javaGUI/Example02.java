package javaGUI;

import javax.swing.*;
import javax.swing.*;

public class Example02 {
	JFrame jf = new JFrame("简单表格");
	JTable table;
	Object[] columnTitle = { "姓名", "年龄", "姓名" };
	Object[][] tableDate = { new Object[] { "李清照", 29, "女" },
			new Object[] { "苏格拉底", 56, "男" },
			new Object[] { "李白", 35, "男" },
			new Object[] { "弄玉", 18, "女" }, 
			};

	public void init() {
		table = new JTable(tableDate, columnTitle);
		jf.add(new JScrollPane(table));
		jf.pack();
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setVisible(true);

	}

	public static void main(String[] args) {
		new Example02().init();
	}

}
