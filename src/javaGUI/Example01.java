package javaGUI;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class Example01 extends JFrame{
		private ButtonGroup group;
		private JPanel panel;
		private JPanel pallet;
		public Example01() {
			pallet = new JPanel();
			this.add(pallet,BorderLayout.CENTER);
			panel = new JPanel();
			group = new ButtonGroup();
			
			addJRadioButton("��");
			addJRadioButton("��");
			addJRadioButton("��");
			this.add(panel, BorderLayout.SOUTH);
			this.setSize(300,300);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setVisible(true);
		}
		private void addJRadioButton(final String text) {
			JRadioButton radioButton = new JRadioButton(text);
			group.add(radioButton);
			panel.add(radioButton);
			radioButton.addActionListener(new ActionListenner() {
				public void actionPerformed(ActionEvent e) {
					Color color = null;
					if("��".equals(text)) {
						color = Color.GRAY;
					}else if ("��".equals(text)) {
						color = Color.pink;
					}else if ("��".equals(text)) {
						color = Color.YELLOW;
					}else {
						color = Color.BLACK;
					}
					pallet.setBackground(color);
					}
				});
			}
			public static void main(String[] args) {
				new Example01();
			
			
		}

}