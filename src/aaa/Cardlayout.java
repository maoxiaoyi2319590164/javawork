package aaa;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

//定义类时实现监听接口

@SuppressWarnings("serial")
public class Cardlayout extends Frame implements ActionListener {

	Button nextbutton;

	Button preButton;

	Panel cardPanel = new Panel();

	Panel controlpaPanel = new Panel();

	// 定义卡片布局对象

	CardLayout card = new CardLayout();

	// 定义构造函数

	public Cardlayout() {

		super();

		setSize(300, 200);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setLocationRelativeTo(null);

		setVisible(true);

		// 设置cardPanel面板对象为卡片布局

		cardPanel.setLayout(card);

		// 循环，在cardPanel面板对象中添加五个按钮

		// 因为cardPanel面板对象为卡片布局，因此只显示最先添加的组件

		for (int i = 0; i < 5; i++) {

			cardPanel.add(new Button("按钮" + i));

		}

		// 实例化按钮对象

		nextbutton = new Button("下一张卡片");

		preButton = new Button("上一张卡片");

		// 为按钮对象注册监听器

		nextbutton.addActionListener((ActionListener) this);

		preButton.addActionListener((ActionListener) this);

		controlpaPanel.add(preButton);

		controlpaPanel.add(nextbutton);

		// 定义容器对象为当前窗体容器对象

		Container container = getContentPane();

		// 将 cardPanel面板放置在窗口边界布局的中间，窗口默认为边界布局

		container.add(cardPanel, BorderLayout.CENTER);

		// 将controlpaPanel面板放置在窗口边界布局的南边，

		container.add(controlpaPanel, BorderLayout.SOUTH);

	}

	// 实现按钮的监听触发时的处理

	private Container getContentPane() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unused")
	private void setLocationRelativeTo(Object object) {
		// TODO Auto-generated method stub

	}

	private void setDefaultCloseOperation(int exitOnClose) {
		// TODO Auto-generated method stub

	}

	public void actionPerformed(ActionEvent e) {

		// 如果用户单击nextbutton，执行的语句

		if (e.getSource() == nextbutton) {

			// 切换cardPanel面板中当前组件之后的一个组件

			// 若当前组件为最后添加的组件，则显示第一个组件，即卡片组件显示是循环的。

			card.next(cardPanel);

		}

		if (e.getSource() == preButton) {

			// 切换cardPanel面板中当前组件之前的一个组件

			// 若当前组件为第一个添加的组件，则显示最后一个组件，即卡片组件显示是循环的。

			card.previous(cardPanel);

		}

	}

	public static void main(String[] args) {

	}

}
