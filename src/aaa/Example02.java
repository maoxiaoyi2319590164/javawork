package aaa;
import java.awt.*;
import java.awt.event.*;
public class Example02 {
	
	
	public static void main(String[] args) {
		//新建窗口
		Frame f = new Frame("我的窗口！");
		//设置窗口的宽与高
		f.setSize(400,300);
		//设置窗口出现的位置
		f.setSize(300,200);
		//设置窗口可见
		f.setVisible(true);
		Button btnl = new Button("press");
		btnl.setSize(300,100);
		btnl.setLocation(100,100);
		f.add(btnl);
		f.setVisible(true);
		btnl.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				super.mouseClicked(e);
				System.out.println(e.getButton()+""+e.getX() +""+e.getY());
			}
		});
		MyWindowListener mw = new MyWindowListener();
		f.addWindowListener(mw);
		
	}

}
//创建MyWindowListener类实现WindowListener接口
class MyWindowListener implements WindowListener {
	//监听器监听事件对象作出处理
	public void windowClosing(WindowEvent e){
		Window window = e.getWindow();
		window.setVisible(false);
		//释放窗口
		window.dispose();
			
	}
	public void windowActivated(WindowEvent e){
		
	}
	public void windowClosed(WindowEvent e){
		
	}
	@Override
	public void windowDeactivated(WindowEvent e) {
		
	}
	@Override
	public void windowDeiconified(WindowEvent e) {
		
		
	}
	@Override
	public void windowIconified(WindowEvent e) {
		
		
	}
	@Override
	public void windowOpened(WindowEvent e) {
		
		
	}
}