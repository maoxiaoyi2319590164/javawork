package jdbc;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcDemo02 {

	/**
	 * @param args
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://127.0.0.1:3307/jdbc";
		String username = "root";
		String password = "root";
	    
		Connection con = DriverManager.getConnection(url, username, password);
		Statement stm = con.createStatement();
		String sql = "select * from users";
		ResultSet rs = stm.executeQuery(sql);
		
		while(rs.next()){
			int id = rs.getInt("id");
			String name = rs.getString("name");
			System.out.println(id + "=="+name);
			
		}
		rs.close();
		stm.close();
		con.close();
	}

}
