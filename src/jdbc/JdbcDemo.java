package jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
public class JdbcDemo {

	
	public static void main(String[] args) throws ClassNotFoundException,SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3307/jdbc";
		String username = "root";
		String password = "root";
		Connection con = DriverManager.getConnection(url, username, password);
		System.out.println("连接成功！");
		String sql = "insert into users(name,passwoed,email,birthday)" + 
		"values('zhaoliu','123456','zhaoliu@sina.com','1999-9-9')";
		Statement stm = con.createStatement();
		stm.executeUpdate(sql);
		stm.close();
		con.close();
		
		System.out.println("关闭连接");
	}

}

