package game;


import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;
import java.util.TreeSet;

public class Poker {
    public static void main(String[] args) {

        //1.创建一个TreeMap集合来存储扑克牌编号和扑克牌花色及点数
        TreeMap<Integer, String> pokers = new TreeMap<Integer,String>();

        //2.定义两个数组分别用来存储花色和点数
        String[] colors={"♥","♠","♦","♣"};
        String[] numbers={"3","4","5","6","7","8","9","10","J","O","K","A","2"};

        //3.创建一个ArrayList集合来存储编号
        ArrayList<Integer> indexs = new ArrayList<Integer>();

        //4.往HashMap集合中存储编号以及对应的扑克牌，同时往ArrayList集合中存储编号
        int index=0;
        for (String number : numbers) {
            for (String color : colors) {
                pokers.put(index,color+number);
                indexs.add(index);
                index++;
            }
        }
        pokers.put(index,"小王");
        indexs.add(index);
        index++;
        pokers.put(index,"大王");
        indexs.add(index);

        //5.洗牌（洗的是编号）
        Collections.shuffle(indexs);

        //6.发牌（发的也是编号，为了保证扑克牌排好序，创建TreeSet集合来接收）
        TreeSet<Integer> zhangsan = new TreeSet<Integer>();
        TreeSet<Integer> lisi = new TreeSet<Integer>();
        TreeSet<Integer> wangwu = new TreeSet<Integer>();
        TreeSet<Integer> dipai = new TreeSet<Integer>();
        for (int i = 0; i < indexs.size(); i++) {
            if(i>=indexs.size()-3){
                dipai.add(indexs.get(i));
            }else if(i%3==0){
                zhangsan.add(indexs.get(i));
            }else if(i%3==1){
                lisi.add(indexs.get(i));
            }else if(i%3==2){
                wangwu.add(indexs.get(i));
            }
        }

        //7.看牌（遍历TreeSet集合，获取编号，到HashMap集合中找对应的牌）
        lookPoker("张三",zhangsan,pokers);
        lookPoker("李四",lisi,pokers);
        lookPoker("王五",wangwu,pokers);
        lookPoker("底牌",dipai,pokers);
    }

    private static void lookPoker(String name, TreeSet<Integer> indexs,
            TreeMap<Integer, String> pokers) {
        System.out.print(name+"的牌为：");
        for (Integer index : indexs) {
            System.out.print(pokers.get(index)+"  ");
        }
        System.out.println();
    }

}

